#!/bin/sh

REPO=https://gitlab.com/hugovinicius/conchayoro
AMBIENTE_TESTE_CONFIGURADO=False
AMBIENTE_PRODUCAO_CONFIGURADO=False

NODE1=ip172-18-0-66-bliqtn4gl2300085tivg@direct.labs.play-with-docker.com
NODE2=ip172-18-0-77-bliqtn4gl2300085tivg@direct.labs.play-with-docker.com
NODE3=ip172-18-0-78-bliqtn4gl2300085tivg@direct.labs.play-with-docker.com
NODE4=ip172-18-0-79-bliqtn4gl2300085tivg@direct.labs.play-with-docker.com

if [ -z "$1" ]; then

  HOST_LOCAL=False

else

  HOST_LOCAL=True

fi

if [ $HOST_LOCAL = True ]; then

  echo "Configuração do ambiente local"
  
  docker-compose --compatibility up -d jenkins nexus sonar web db

else

  echo "Configuração do ambiente remoto"

  docker-compose --compatibility up -d jenkins

  ssh -t $NODE2 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d nexus"

  ssh -t $NODE3 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d sonar"

  ssh -t $NODE4 "git clone $REPO && cd conchayoro/ambiente && docker-compose --compatibility up -d web db" 

  if [ $AMBIENTE_TESTE_CONFIGURADO = True ];then

     ssh -t $NODE5@$HOST "git clone $REPO && cd conchayoro/ambiente && docker-compose -f docker-compose-teste.yml --compatibility up -d web db"

  fi

fi
