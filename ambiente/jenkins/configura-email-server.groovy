import jenkins.model.*

def inst = Jenkins.getInstance()

def desc = inst.getDescriptor("hudson.tasks.Mailer")

desc.setReplyToAddress(System.getenv('EMAIL_REPLY_ADDRESS'))
desc.setSmtpHost(System.getenv('EMAIL_SMTP_HOST'))
desc.setSmtpPort(System.getenv('EMAIL_SMTP_PORT'))
desc.setSmtpAuth(System.getenv('EMAIL_SMTP_USER'), System.getenv('EMAIL_SMTP_PASSWORD'))
desc.setUseSsl(false)
desc.setCharset("UTF-8")

desc.save()